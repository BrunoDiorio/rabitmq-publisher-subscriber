package dao;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class LogManager {
	
	private File open() throws IOException {
		
		LocalDate date = LocalDate.now();
		
		File f = new File( date.format(DateTimeFormatter.BASIC_ISO_DATE) + ".log" );
		
		if(!f.exists())
			f.createNewFile();
		
		return f;
	}

	
	private void logMsg(String msg ) {
		try {
			File f = new LogManager().open();
			
			PrintWriter pw = new PrintWriter(new FileOutputStream(f, true));
			
			pw.println(msg);
			pw.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void log( String msg) {
		new LogManager().logMsg(msg);
	}
}
