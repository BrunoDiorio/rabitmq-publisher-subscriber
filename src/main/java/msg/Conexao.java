package msg;

import java.io.IOException;
import java.net.Inet4Address;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class Conexao {

	private static final String EXCHANGE_NAME = "fanout_logs";
	private static ConnectionFactory factory;

	private Connection connection;
	private Channel channel;

	static {
		try {
			factory = new ConnectionFactory();
			factory.setHost("192.168.1.106");
			factory.setUsername("admin");
			factory.setPassword("admin");
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	private void open() throws Exception {
		connection = factory.newConnection();
		channel = connection.createChannel();
		channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.FANOUT);//TIPO FAN OUT
	}

	private void close() throws IOException, TimeoutException {
		channel.close();
		connection.close();
	}

	public void send(String threadName, Severity severity) throws Exception {
		open();
		StringBuilder sb = new StringBuilder( );
				 
		sb.append( "\"address\": \""  ).append(Inet4Address.getLocalHost()).append( "\", " )
		  .append( "\"thread\": \""   ).append(threadName).append( "\", " )
		  .append( "\"date\": \""     ).append(LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME)).append("\", ")
		  .append( "\"severity\": \"" ).append( severity.name() ).append("\"");
		
		channel.basicPublish(EXCHANGE_NAME, "", null, sb.toString().getBytes());
		System.out.println("[x] Send { " + sb + " }" );
		close();
	}
	
	public Consumer getConsumer() throws Exception {
		open();
		channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.FANOUT);
		String queueName = channel.queueDeclare().getQueue();
		
		channel.queueBind(queueName, EXCHANGE_NAME, "");
		
		Consumer consumer = new Consumer(channel);
		channel.basicConsume(queueName, true, consumer);
		System.out.println("[x] Consumer Started");
		return consumer;		
	}
}
