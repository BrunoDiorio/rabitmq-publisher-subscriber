package msg;

import java.io.IOException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.AMQP.BasicProperties;

import dao.LogManager;

public class Consumer extends DefaultConsumer{

	public Consumer(Channel channel) {
		super(channel);
	}

	@Override
	public void handleDelivery(String consumerTag, Envelope envelope, BasicProperties properties, byte[] body) throws IOException {
		String message = new String(body, "UTF-8");
		
		System.out.println(" [x] Received { " + message + " }");
		
		LogManager.log( "{ " + message + " }," );
	}
}
