package tst;

import msg.Conexao;
import msg.Severity;

public class Send {

	public static void main(String[] args) {

		new Tst().start();
	}
	
}

class Tst extends Thread{
	
	@Override
	public void run() {
		try {
			
			Conexao c = new Conexao();
			
			c.send(this.getName(), Severity.ERRO_PARCIAL);
			c.send(this.getName(), Severity.ERRO_TOTAL);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
